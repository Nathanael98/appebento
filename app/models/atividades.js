import DS from 'ember-data';

export default DS.Model.extend({

    titulo: DS.attr('string'),
    descripcion: DS.attr('string'),
    persona: DS.hasMany('persona')     

});
