import Controller from '@ember/controller';


export default Controller.extend({
    actions: {
        registrarPersona() {
            var person = this.store.createRecord('persona', {
                    
                description: this.descrip,
                email: this.correo,
                ids: this.codigo,
                img: this.imagen,
                job: this.ocupacion,
                name: this.nombre,
                social: this.redsocial, 
            });
            person.save();
        },
    }
});
