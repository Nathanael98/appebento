import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { validator, buildValidations } from 'ember-cp-validations';

/* Validaciones... */
const Validations = buildValidations({


    'user': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('length', {
            max: 12,
            min: 4,
            message: 'Nombre de usuario no valido'
        })
    ],

    'interes': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('length', {
            max: 30,
            message: 'No debe de excederce a mas de 30 caracteres'
        })
    ],


});


export default Controller.extend(Validations, {    

    actions: {

        select(post) {
            console.log(post.get('title'));
          },
       
        toogleError(attr) {
            switch (attr) {
                case 'user':
                    this.set('userErrorCheck', true)
                    break
                case 'interes':
                    this.set('interesErrorCheck', true)
                    break
            }
        },
        
    }
});
