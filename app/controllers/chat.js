import Controller from '@ember/controller';
import { validator, buildValidations } from "ember-cp-validations";


const Validations = buildValidations({
    'txtArea': 
    validator('presence', {
        presence: true,
        message: 'Escribe texto primero'
    })

})

export default Controller.extend(Validations, {
    actions: {
        createMensaje() {
            let mensaje = this.store.createRecord('chat', {
                messag: this.txtArea

            })
            mensaje.save()


        },

        toggleError(attr) {
            switch (attr) {
                case 'txtArea':
                    this.set('txtAreaErrorCheck', true)


            }

        }

    }
});