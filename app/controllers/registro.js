import Controller from '@ember/controller';

import { inject as service } from '@ember/service';
import { validator, buildValidations } from 'ember-cp-validations';


const Validations = buildValidations({
    'usuario': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('format', {
            type: 'email',
            message: 'Esto no es un correo'
        })
    ],
    'pass': [
        validator('presence', {
            presence: true,
            message: 'El campo no puede estar vacio'
        }),
        validator('length', {
            min: 6,
        })
    ],
    'confirmPass': [
        validator('confirmation', {
            on: 'pass',
            message: 'Las contraseñas no coinciden'
        })
    ]
});

export default Controller.extend(Validations, {

    //fiebaseApp: service(),
    session: service(),
    

    actions: {

        toogleError(attr) {
            switch (attr) {
                case 'usuario':
                    this.set('userErrorCheck', true)
                    break
                case 'pass':
                    this.set('passwordErrorCheck', true)
                    break
                case 'confirmPass':
                    this.set('confirmPasswordErrorCheck', true)
                    break
            }
        },

        /* Autenticacion FireBase... */
        
               /* registrar(user, pass) {
                    this.get('firebaseApp').auth().createUserWithEmailAndPassword(user, pass)
                        .then(() => {
        
                            var user = this.get('firebaseApp').auth().currentUser;
                            user.sendEmailVerification().then(function () {
                                
                                console.log('Enviando Correo...')
        
                            }).catch(function(error){
        
                                console.log(error)
                            });
                        }).catch(function(error){
                            var errorCode = error.code;
                            var errorMessage = error.message;
        
                            console.log("Error" + error1)
                            console.log("Error 2" + errorMessage)
                        })
                },*/


        loginRegistro(user, pass) {
            console.log(user)
            console.log(pass)
            if (user.search('@') == -1) {
                alert('No es un correo')
            }
            this.get('session').open('firebase', {
                provider: 'password',
                email: user,
                password: pass
            }).then(() => {
                //si
                alert('Usuario autenticado')
                this.transitionToRoute('logged.home')
            }).catch((e) => {
                console(e)
                //no
                alert('Usuario no autenticado')
            })
        }

    }
});
