import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | exampledos', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:exampledos');
    assert.ok(route);
  });
});
